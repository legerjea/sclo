
# Fichiers

 - `sclo.sty`: tout ce qu'il faut
 - fichier source (s'inspirer de `exemple.tex`)

# Dépendances

TL;DR; (pour débiane et dérivées):

```
apt-get install texlive-full auto-multiple-choice fonts-opendyslexic fonts-texgyre
```

## TeX and co

Besoin de XeLaTeX et LuaLaTeX, et besoin des paquets les plus classiques. Sous
débiane et dérivées, instalation de `texlive-full` recommandée.

## AMC

Installer AMC. Sous débiane, le paquet se nomme `auto-multiple-choice`.

## Fontes opendyslexic et compagnie

On a besoin de la fonte `opendyslexic`, mais aussi de la fonte
`texgyrepagella-math` pour la version dyslexic (puisque les maths sont composés
avec texgyrepagella à l'exception des symboles romains et chiffres qui sont
composés avec opendyslexic.

Sous débiane, utiliser les paquets `fonts-opendyslexic` et `fonts-texgyre`.

# Élaboration du sujet

Je considère ici que le fichier source se nomme `examen.tex`

 - Faire le sujet, le compiler en utilisant au moins deux fois:

   ```
   xelatex examen.tex
   ```

 - Vérifier la version énoncé (pour les annales), en utilisant:

   ```
   xelatex -jobname examen_enonce examen.tex
   ```

 - Et le corrigé avec:

   ```
   xelatex -jobname examen_corrige examen.tex
   ```

Attention, c'est dans le sujet qu'il faut préciser le nombre d'exemplaires de
chaque type. Pour réduire le temps d'élaboration, c'est à mettre à 1 au début
puis à changer au dernier moment.

# Sujets aménagés

Il y a 6 configurations pour les sujets, il faut indiquer le nombre de chaque
config (par défaut c'est zéro):

 - `normal`: tout en Libertinus, justifié, interlignage normal, taille des boites
   normales, pour 2 exemplaires `\SCLOnumnormal{20}`.
 - `big`: tout en Libertinus, justifié, interlignage normal, taille des boites augmenté,
   pour 2 exemplaires `\SCLOnumbig{2}`.
 - `sans`: tout en fonte sans empattements, justifié, interlignage augmenté, taille des
   boites augmentée, pour 2 exemplaires \SCLOnumsans{2}
 - `sansrr`: tout en fonte sans empattements, aligné à gauche, interlignage augmenté, taille des
   boites augmentée, pour 2 exemplaires \SCLOnumsans{2}
 - `dys`: tout en Opendyslexic, justifié, interlignage augmenté, taille des boites
   augmentée, pour 2 exemplaires `\SCLOnumdys{2}`.
 - `dysrr`: tout en Opendyslexic, aligné à gauche, interlignage augmenté, taille des boites
   augmentée, pour 2 exemplaires `\SCLOnumdys{2}`.

Les sujets sont imprimés dans cet ordre (pour des raisons obscures d'ecrasement
de police, il n'est pas possible de faire autrement).

# AMC


## Faire un projet

 - Lancer AMC
 - Démarer un nouveau projet, lui donner un nom (`truc_muche`)
 - Choisir `Fichier`
 - Sélectionner le fichier tex ellaboré à la partie précédente.
 - Aller dans le repertoire projet, virez le fichier qu'il a copié, faire un
   lien symbolique, et liez le fichier `sclo.sty` ainsi que tous les autres
   fichiers que vous pouriez avoir besoin
   ```
   cd ~/Projets-QCM/truc_muche
   ln -s SOMEWHERE/examen.tex .
   ln -s SOMEWHERE/sclo.sty .
   …
   ```
 - Ouvrir les préférences, onglet projet, apporter les modifications suivantes:
   - seuil de noirceur : `0.05`
   - Annotation des copies, texte d'en-tête: *vider le champ*
   - Anontation des questions: *vider le champ*
   - Texte d'annotation des questions annulées: *vider le champ*
   - Moteur LaTeX: `lualatex` (oui, pas `xelatex` qui semble avoir des problèmes
     avec la détection des boites).

## Faire le sujet à imprimer

 - Cliquer sur mettre à jour les documents (*Attention, si la compilation a été
   lancée par erreur avec pdflatex, il faudra supprimer à la main le fichier aux
   pour ne pas avoir d'erreur étranges.*)
 - Cliquez sur calculer les mises en page
 - Consulter les détéctions (à droite du xxxx pages traitées), et vérifier la
   détections des boites sur toutes les pages (pas tous les sujets, mais au
   moins toutes les pages d'un sujet).
 - Si mauvaises détections, remettre à joure les documents (bien que mettre à
   jour les documents lance deux compilations, il semble qu'il fasse
   les mettre à jour deux fois dans certains cas).
 - Récupérer le fichier `DOC-sujet.pdf`, attention les versions normales, big,
   et opendyslexic ont potentiellement un nombre de page différent, ce qui pose
   problème à l'imprimerie. Découper le fichier en deux ou en trois, pour que dans
   chaque fichier, le nombre de page par sujet soit le même.
   ```
   pdftk DOC-sujet.pdf cat 1-1234 output sujet-part1.pdf
   pdftk DOC-sujet.pdf cat 1235-1278 output sujet-part2.pdf
   ```

À partir du moment où les fichiers ont été transmis à l'imprimerie, les
documents ne doivent jamais être mis-à-jour.


 
